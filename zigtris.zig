const std = @import("std");
const zbox = @import("./zbox/src/box.zig");

const BOARD_WIDTH: usize = 10;
const BOARD_HEIGHT: usize = 20;

const BOARD_TOP: usize = 1;
const BOARD_LEFT: usize = 10;

const Tile = enum {
    empty, c, y, p, g, r, b, o
};

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    try zbox.init(allocator);
    defer zbox.deinit();

    // die on ctrl+C
    try zbox.handleSignalInput();

    try zbox.cursorHide();

    //setup our drawing buffer
    var size = try zbox.size();

    var output = try zbox.Buffer.init(allocator, size.height, size.width);
    defer output.deinit();

    var board: [BOARD_HEIGHT][BOARD_WIDTH]Tile = undefined;
    for (&board) |*row| {
        for (&row.*, 0..) |*tile, i| {
            tile.* = @intToEnum(Tile, i % @typeInfo(Tile).Enum.fields.len);
        }
    }

    while (true) {
        // update the size of output buffer
        size = try zbox.size();
        try output.resize(size.height, size.width);

        output.clear();
        var cursor = output.cursorAt(BOARD_TOP, BOARD_LEFT);
        var w = cursor.writer();

        try w.writeAll("╔");
        try draw_horizontal(w);
        try w.writeAll("╗");

        var row: usize = 0;
        while (row < BOARD_HEIGHT) : (row += 1) {
            cursor = output.cursorAt(BOARD_TOP + row + 1, BOARD_LEFT);
            w = cursor.writer();
            try w.writeAll("║");

            var col: usize = 0;
            while (col < BOARD_WIDTH) : (col += 1) {
                cursor = output.cursorAt(BOARD_TOP + row + 1, BOARD_LEFT + col*2 + 1);
                const tile = board[row][col];
                switch (tile) {
                    .empty => {},
                    .c => cursor.attribs.fg_cyan = true,
                    .y => cursor.attribs.fg_yellow = true,
                    .p => cursor.attribs.fg_magenta = true,
                    .g => cursor.attribs.fg_green = true,
                    .r => cursor.attribs.fg_red = true,
                    .b => cursor.attribs.fg_blue = true,
                    .o => cursor.attribs.fg_orange = true,
                }
                w = cursor.writer();
                if (tile == .empty) {
                    try w.writeAll("  ");
                } else {
                    try w.writeAll("▧ ");
                }
            }
            cursor = output.cursorAt(BOARD_TOP + row + 1, BOARD_LEFT + BOARD_WIDTH*2 + 1);
            w = cursor.writer();
            try w.writeAll("║");
        }

        cursor = output.cursorAt(BOARD_TOP + BOARD_HEIGHT + 1, BOARD_LEFT);
        w = cursor.writer();

        try w.writeAll("╚");
        try draw_horizontal(w);
        try w.writeAll("╝");

        try zbox.push(output);

        std.os.nanosleep(0, 80_000_000);
    }
}

fn draw_horizontal(w: zbox.Buffer.Writer) !void {
    var i: usize = 0;
    while (i < BOARD_WIDTH * 2) : (i += 1) {
        try w.writeAll("═");
    }
}
